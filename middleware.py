from flask import Flask, jsonify, abort, make_response,render_template, flash, request, url_for
from provider_service import *

db_engine = 'sqlite:////Users/mathieulecampion/Documents/MyDigitalSchool/Python/TPFlask/database.db'
dataProviderService = DataProviderService(db_engine)

def index():
	return render_template('index.html', selected_menu_item="index")

def getApiUtilisateur():
	utilisateurs = dataProviderService.get_utilisateur(serialize=True)
	if utilisateurs:
		return jsonify(utilisateurs)
	else:
		abort(404)

def getApiHotel():
	hotels = dataProviderService.get_hotel(serialize=True)
	if hotels:
		return jsonify(hotels)
	else:
		abort(404)

def getApiCommande():
	commandes = dataProviderService.get_commande(serialize=True)
	if commandes:
		return jsonify(commandes)
	else:
		abort(404)

def getApiChambre():
	chambres = dataProviderService.get_chambre(serialize=True)
	if chambres:
		return jsonify(chambres)
	else:
		abort(404)

def getApiSejour():
	sejours = dataProviderService.get_sejour(serialize=True)
	if sejours:
		return jsonify(sejours)
	else:
		abort(404)

def getApiCategorie():
	categories = dataProviderService.get_categorie(serialize=True)
	if categories:
		return jsonify(categories)
	else:
		abort(404)

def getApiClient():
	clients = dataProviderService.get_client(serialize=True)
	if clients:
		return jsonify(clients)
	else:
		abort(404)

def getApiUtilisateurId(id):
	if type(id) == int:
		utilisateur = dataProviderService.get_utilisateur(id,serialize=True)
		if utilisateur:
			return jsonify(utilisateur)
		else:
			abort(404)
	else:
		abort(400)
	
def getApiInterviews():
	interviews = dataProviderService.get_interview(serialize=True)
	if interviews:
		return jsonify(interviews)
	else:
		abort(404)

def getApiPositions():
	positions = dataProviderService.get_position(serialize=True)
	if positions:
		return jsonify(positions)
	else:
		abort(404)
	
def postApiCandidates():
	first_name = request.form["first_name"]
	last_name = request.form["last_name"]
	email = request.form["email"]
	if first_name is not None and last_name is not None and email is not None:
		candidates = dataProviderService.add_candidate(first_name, last_name, email)
		if candidates:
			return make_response(jsonify({'id': candidates,'url': url_for('apiGetCandidate', id=candidates)}),201)
		else:
			abort(500)
	else:
		abort(400)

def initDb():
	dataProviderService.init_database()
	return jsonify({"message":"Instantiation de la BDD Ok !"})

def fillDb():
	dataProviderService.fill_database()
	return jsonify({"message":"Fill de la BDD Ok !"})