from flask import Flask, jsonify, abort, make_response,render_template
#from quotes import funny_quotes
import random
from candidat_fixtures import *
from data_provider_service import *
from routes import *

app = Flask(__name__)

routingApi(app)
# routingHtml(app)
init_error_handlers(app)
init_db(app)

if __name__ == "__main__":
	app.secret_key = 'suk43RFDffR23'
	app.run(debug=True)