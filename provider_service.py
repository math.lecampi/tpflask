from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import datetime

from models.Categorie import Categorie
from models.Chambre import Chambre
from models.Client import Client
from models.Commande import Commande
from models.Hotel import Hotel
from models.Sejour import Sejour
from models.Utilisateur import Utilisateur
from models.Service import Service
from models.init_database import init_database



class DataProviderService:
    def __init__(self, engine):
        """
        :param engine: The engine route and login details
        :return: a new instance of DAL class
        :type engine: string
        """
        if not engine:
            raise ValueError('The values specified in engine parameter has to be supported by SQLAlchemy')
        self.engine = engine
        db_engine = create_engine(engine)
        db_session = sessionmaker(bind=db_engine)
        self.session = db_session()

    def init_database(self):
        """
        Initializes the database tables and relationships
        :return: None
        """
        init_database(self.engine)

    def add_utilisateur(self, login, password, nom, prenom, phone=None, languages="", skills=""):

        new_candidate = Candidate(first_name=first_name,
                                  last_name=last_name,
                                  email=email,
                                  birthday=birthday,
                                  phone=phone,
                                  languages=languages,
                                  skills=skills)

        self.session.add(new_candidate)
        self.session.commit()

        return new_candidate.id

    def get_utilisateur(self, id=None, serialize=False):

        all_utilisateurs = []

        if id is None:
            all_utilisateurs = self.session.query(Utilisateur).order_by(Utilisateur.login).all()
        else:
            all_utilisateurs = self.session.query(Utilisateur).filter(Utilisateur.id == id).all()

        if serialize:
            return [usr.serialize() for usr in all_utilisateurs]
        else:
            return all_utilisateurs
        
    def get_hotel(self, id=None, serialize=False):

        all_hotels = []

        if id is None:
            all_hotels = self.session.query(Hotel).order_by(Hotel.nom).all()
        else:
            all_hotels = self.session.query(Hotel).filter(Hotel.id == id).all()

        if serialize:
            return [usr.serialize() for usr in all_hotels]
        else:
            return all_hotels

    def get_commande(self, id=None, serialize=False):

        all_commandes = []

        if id is None:
            all_commandes = self.session.query(Commande).order_by(Commande.date).all()
        else:
            all_commandes = self.session.query(Commande).filter(Commande.id == id).all()

        if serialize:
            return [usr.serialize() for usr in all_commandes]
        else:
            return all_commandes

    def get_chambre(self, id=None, serialize=False):

        all_chambres = []

        if id is None:
            all_chambres = self.session.query(Chambre).order_by(Chambre.nom).all()
        else:
            all_chambres = self.session.query(Chambre).filter(Chambre.id == id).all()

        if serialize:
            return [usr.serialize() for usr in all_chambres]
        else:
            return all_chambres
        
    def get_sejour(self, id=None, serialize=False):

        all_sejours = []

        if id is None:
            all_sejours = self.session.query(Sejour).order_by(Sejour.date_arrive).all()
        else:
            all_sejours = self.session.query(Sejour).filter(Sejour.id == id).all()

        if serialize:
            return [usr.serialize() for usr in all_sejours]
        else:
            return all_sejours
    
    def get_categorie(self, id=None, serialize=False):

        all_categories = []

        if id is None:
            all_categories = self.session.query(Categorie).order_by(Categorie.nom).all()
        else:
            all_categories = self.session.query(Categorie).filter(Categorie.id == id).all()

        if serialize:
            return [usr.serialize() for usr in all_categories]
        else:
            return all_categories

    def get_client(self, id=None, serialize=False):

        all_clients = []

        if id is None:
            all_clients = self.session.query(Client).order_by(Client.nom).all()
        else:
            all_clients = self.session.query(Client).filter(Client.id == id).all()

        if serialize:
            return [usr.serialize() for usr in all_clients]
        else:
            return all_clients
    


    def update_candidate(self, id, new_candidate):
        updated_candidate = None
        candidate = self.get_candidate(id)[0]

        if candidate:
            candidate.email = new_candidate["email"]
            candidate.phone = new_candidate["phone"]
            candidate.first_name = new_candidate["first_name"]
            candidate.last_name = new_candidate["last_name"]
            self.session.add(candidate)
            self.session.commit()
            updated_candidate = self.get_candidate(id)[0]

        return updated_candidate

    def delete_candidate(self, id):
        if id:
            items_deleted = self.session.query(Candidate).filter(Candidate.id == id).delete()
            return items_deleted > 0
        return False 


    def fill_database(self):

        #
        # Hotel
        #
        hotel1 = Hotel(nom="Hotel 1")
        hotel2 = Hotel(nom="Hotel 2")
        hotel3 = Hotel(nom="Hotel 3")

        self.session.add(hotel1)
        self.session.add(hotel2)
        self.session.add(hotel3)
        self.session.commit()

        #
        # Utilisateurs
        #
        cand1 = Utilisateur(login="login1",
                        password="password1",
                        nom="Doe",
                        prenom="Jane",
                        telephone="0000000000",
                        email="doe.jane@gmail.com",
                        adresse='2 rue du pyton',
                        estActif=True,
                        service = Service.DIR,
                        hotel = hotel1.id)
        cand2 = Utilisateur(login="login2",
                        password="password2",
                        nom="Doe",
                        prenom="Jane",
                        telephone="0000000000",
                        email="doe.jane@gmail.com",
                        adresse='2 rue du pyton',
                        estActif=True,
                        service = Service.REC,
                        hotel = hotel2.id)

        self.session.add(cand1)
        self.session.add(cand2)
        self.session.commit()

        #
        # Catégorie
        #

        cat1 = Categorie(nom = "Simple",
                        prix = 10.0)
        cat2 = Categorie(nom = "Confort",
                        prix = 20.0)
        cat3 = Categorie(nom = "Moyen-confort",
                        prix = 40.0)
        cat4 = Categorie(nom = "Suite-junior",
                        prix = 50.0)
        cat5 = Categorie(nom = "Suite-senior",
                        prix = 60.0)

        self.session.add(cat1)
        self.session.add(cat2)
        self.session.add(cat3)
        self.session.add(cat4)
        self.session.add(cat5)
        self.session.commit()


        #
        # Chambre
        #

        chb1 = Chambre(nom = "Chambre 1",
                        hotel = hotel1.id,
                        categorie = cat1.id)
        chb2 = Chambre(nom = "Chambre 2",
                        hotel = hotel1.id,
                        categorie = cat2.id)
        chb3 = Chambre(nom = "Chambre 3",
                        hotel = hotel1.id,
                        categorie = cat3.id)
        chb4 = Chambre(nom = "Chambre 4",
                        hotel = hotel1.id,
                        categorie = cat4.id)
        chb5 = Chambre(nom = "Chambre 5",
                        hotel = hotel1.id,
                        categorie = cat5.id)

        self.session.add(chb1)
        self.session.add(chb2)
        self.session.add(chb3)
        self.session.add(chb4)
        self.session.add(chb5)
        self.session.commit()

        #
        # Client
        #

        cl1 = Client(nom = "Client nom 1",
                    prenom = "Client prenom 1",
                    telephone = "00000000",
                    num_carte_bancaire = "12345667788990674",
                    adresse = "Adresse 1")
        cl2 = Client(nom = "Client nom 2",
                    prenom = "Client prenom 3",
                    telephone = "00000000",
                    num_carte_bancaire = "987667788990674",
                    adresse = "Adresse 2")

        self.session.add(cl1)
        self.session.add(cl2)
        self.session.commit()


        #
        # Sejour
        #

        sjr1 = Sejour(date_arrive = datetime.date(2019, 6, 13),
                        date_depart = datetime.date(2019, 6, 20),
                        chambre = chb1.id,
                        client = cl1.id,
                        utilisateur = cand1.id)
        sjr2 = Sejour(date_arrive = datetime.date(2019, 3, 13),
                        date_depart = datetime.date(2019, 3, 20),
                        chambre = chb5.id,
                        client = cl2.id,
                        utilisateur = cand2.id)

        self.session.add(sjr1)
        self.session.add(sjr2)
        self.session.commit()

        #
        # Commande
        #

        cmd1 = Commande(date = datetime.date(2019, 3, 13),
                    hotel = hotel1.id,
                    client = cl1.id,
                    produits = "Produit 1;Produit 2")
        cmd2 = Commande(date = datetime.date(2019, 5, 13),
                    hotel = hotel1.id,
                    client = cl2.id,
                    produits = "Produit 3;Produit 4")

        self.session.add(cmd1)
        self.session.add(cmd2)
        self.session.commit()

        
