from middleware_old import *
from middleware import *

def routingApi(app):
    app.add_url_rule('/api/utilisateurs', 'getApiUtilisateur', getApiUtilisateur, methods=['GET'])
    app.add_url_rule('/api/utilisateurs/<int:id>', 'getApiUtilisateurs',getApiUtilisateurId, methods=['GET'])

    app.add_url_rule('/api/hotels', 'getApiHotel', getApiHotel, methods=['GET'])
    #app.add_url_rule('/api/hotels/<int:id>', 'getApiHotel',getApiHotel, methods=['GET'])

    app.add_url_rule('/api/commandes', 'getApiCommande', getApiCommande, methods=['GET'])
    #app.add_url_rule('/api/hotels/<int:id>', 'getApiHotel',getApiHotel, methods=['GET'])

    app.add_url_rule('/api/chambres', 'getApiChambre', getApiChambre, methods=['GET'])
    #app.add_url_rule('/api/hotels/<int:id>', 'getApiHotel',getApiHotel, methods=['GET'])

    app.add_url_rule('/api/sejours', 'getApiSejour', getApiSejour, methods=['GET'])
    #app.add_url_rule('/api/hotels/<int:id>', 'getApiHotel',getApiHotel, methods=['GET'])

    app.add_url_rule('/api/categories', 'getApiCategorie', getApiCategorie, methods=['GET'])
    #app.add_url_rule('/api/hotels/<int:id>', 'getApiHotel',getApiHotel, methods=['GET'])

    app.add_url_rule('/api/clients', 'getApiClient', getApiClient, methods=['GET'])
    #app.add_url_rule('/api/hotels/<int:id>', 'getApiHotel',getApiHotel, methods=['GET'])


    # app.add_url_rule('/api/candidates', 'apiPostCandidats', postApiCandidates, methods=['POST'])
    # app.add_url_rule('/api/interviews', 'apiInterviews', getApiInterviews, methods=['GET'])
    # app.add_url_rule('/api/positions', 'apiPositions', getApiPositions, methods=['GET'])
    # app.add_url_rule('/api/projects', 'apiProjects', getApiProjets, methods=['GET'])
    # app.add_url_rule('/api/experience', 'apiExperiences', getApiExperiences, methods=['GET'])
    # app.add_url_rule('/api/candidate/delete/<string:id>', 'apiDeleteProjects', getApiProjets, methods=['GET'])
    # app.add_url_rule('/api/candidate/<string:id>', 'candidate',getCandidat, methods=['GET'])
    
    
def init_error_handlers(app):
    app.add_url_rule('/crash','crash500',crash500, methods=['GET'])
    app.errorhandler(404)(handle_error_404)
    app.errorhandler(500)(handle_error_500)

def init_db(app):
    app.add_url_rule('/api/initdb','initdb',initDb, methods=['GET'])
    app.add_url_rule('/api/filldb','filldb',fillDb, methods=['GET'])
