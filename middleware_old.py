from flask import Flask, jsonify, abort, make_response,render_template, flash
from data_provider_service import *

dataProviderService = DataProviderService(10)

def index():
	return render_template('index.html', selected_menu_item="index")

#API
# def getApiProjets():
# 	return jsonify(dataProviderService.get_projects())

# def getApiCandidats():
# 	return jsonify(dataProviderService.get_candidates())

# def getApiExperiences():
# 	return jsonify(dataProviderService.get_experiences())

# def deleteCandidateApi(id):
# 	isDeleted = dataProviderService.delete_candidate(id)
# 	print(isDeleted)
# 	if isDeleted:
# 		response = make_response(render_template('index.html'),200)
# 		return response
# 	else:
# 		abort(404)

# def getCandidat(id):
# 	candidate = dataProviderService.get_candidate(id)
# 	if candidate is None:
# 		abort(404)
# 	else:
# 		return jsonify(candidate)


#HTML
def getCandidats():
	return render_template('candidate.html', candidats=dataProviderService.get_candidates())

def getExperiences():
	return render_template('experiences.html', experiences=dataProviderService.get_experiences())

def getProjets():
	return render_template('projects.html', projects=dataProviderService.get_projects())

def crash500():
	abort(500)

def handle_error_404(error):
	flash('Oops! Page not found', 'error')
	return render_template('404.html')

def handle_error_500(error):
	flash('Oops! Internal Server Error', 'error')
	return render_template('500.html')
