from models.Model import Model
from sqlalchemy import Column, Integer, String, Date, Boolean, ForeignKey
from sqlalchemy.orm import relationship

class Client(Model):
    __tablename__ = 'clients'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    nom = Column(String(200), nullable=False)
    prenom = Column(String(200), nullable=False)
    telephone = Column(String(200), nullable=False)
    num_carte_bancaire = Column(String(200), nullable=False)
    adresse = Column(String(200), nullable=False)
    sejour = relationship("Sejour")
    commande = relationship("Commande")

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'prenom':self.prenom,
            'telephone': self.telephone,
            'num_carte_bancaire': self.num_carte_bancaire,
            'adresse': self.adresse,
            'sejour': [srj.serialize() for srj in self.sejour],
            'commande': [cmd.serialize() for cmd in self.commande]
        }