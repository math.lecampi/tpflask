from models.Model import Model
from sqlalchemy import Column, Integer, String, Date, Boolean, ForeignKey, Float
from sqlalchemy.orm import relationship

class Categorie(Model):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    nom = Column(String(200), nullable=False)
    prix = Column(Float, nullable=False)
    chambre = relationship("Chambre")

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'prix': self.prix,
            'chambre': [chb.serialize() for chb in self.chambre]
        }