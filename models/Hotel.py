from models.Model import Model
from sqlalchemy import Column, Integer, String, Date, Boolean, ForeignKey
from sqlalchemy.orm import relationship

class Hotel(Model):
    __tablename__ = 'hotels'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    nom = Column(String(200), nullable=False)
    utilisateur = relationship("Utilisateur")
    commande = relationship("Commande")
    chambre = relationship("Chambre")

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'utilisateur': [usr.serialize() for usr in self.utilisateur],
            'commande': [cmd.serialize() for cmd in self.commande],
            'chambre': [chb.serialize() for chb in self.chambre],
        }