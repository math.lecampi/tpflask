from models.Model import Model
from sqlalchemy import Column, Integer, String, Date, Boolean, ForeignKey
from sqlalchemy.orm import relationship

class Sejour(Model):
    __tablename__ = 'sejours'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    date_arrive = Column(Date, nullable=False)
    date_depart = Column(Date, nullable=False)
    chambre = Column(Integer, ForeignKey('chambres.id'))
    client = Column(Integer, ForeignKey('clients.id'))
    utilisateur = Column(Integer, ForeignKey('utilisateurs.id'))

    def serialize(self):
        return {
            'id':self.id,
            'date_arrive':self.date_arrive,
            'date_depart':self.date_depart,
            'chambre': self.chambre,
            'client': self.client,
            'utilisateur': self.utilisateur
        }