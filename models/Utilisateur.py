from models.Model import Model
from models.Service import Service
from sqlalchemy import Column, Integer, String, Date, Boolean, ForeignKey, Enum
from sqlalchemy.orm import relationship

class Utilisateur(Model):
    __tablename__ = 'utilisateurs'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    login = Column(String(200), nullable=False)
    password = Column(String(200), nullable=False)
    nom = Column(String(200), nullable=False)
    prenom = Column(String(200), nullable=False)
    telephone = Column(String(200), nullable=False)
    email = Column(String(200), nullable=False)
    adresse = Column(String(200), nullable=False)
    estActif = Column(Boolean, nullable=False)
    service = Column(Enum(Service), nullable=False)
    hotel = Column(Integer, ForeignKey('hotels.id'))
    sejour = relationship("Sejour")

    def serialize(self):
        return {
            'id':self.id,
            'login':self.login,
            'password':self.password,
            'nom':self.nom,
            'prenom':self.prenom,
            'telephone':self.telephone,
            'email':self.email,
            'adresse':self.adresse,
            'estActif':self.estActif,
            'service': self.service.value,
            'hotel':self.hotel,
            'sejour': [sjr.serialize() for sjr in self.sejour]
        }