from models.Model import Model
from sqlalchemy import Column, Integer, String, Date, Boolean, ForeignKey
from sqlalchemy.orm import relationship

class Chambre(Model):
    __tablename__ = 'chambres'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    nom = Column(String(200), nullable=False)
    sejour = relationship("Sejour")
    hotel = Column(Integer, ForeignKey('hotels.id'))
    categorie = Column(Integer, ForeignKey('categories.id'))

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'hotel': self.hotel,
            'categorie': self.categorie,
            'sejour': [sjr.serialize() for sjr in self.sejour]
        }