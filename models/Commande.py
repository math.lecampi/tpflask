from models.Model import Model
from sqlalchemy import Column, Integer, String, Date, Boolean, ForeignKey
from sqlalchemy.orm import relationship

class Commande(Model):
    __tablename__ = 'commandes'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    date = Column(Date, nullable=False)
    hotel = Column(Integer, ForeignKey('hotels.id'))
    client = Column(Integer, ForeignKey('clients.id'))
    produits = Column(String(200), nullable=False)

    def serialize(self):
        return {
            'id':self.id,
            'date':self.date,
            'hotel': self.hotel,
            'client': self.client,
            'produits': self.produits
        }