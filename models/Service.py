from models.Model import Model
from enum import Enum

class Service(Enum):
    DIR = "Direction"
    REC = "Reception"
    BAR = "Bar"
    COM = "Comptabilité"